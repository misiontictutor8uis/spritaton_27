const {Schema, model} = require('mongoose')

const usuarioSchema = new Schema({username: String, password:String, telefono:Number})

const Usuario = model('usuario', usuarioSchema)

module.exports = Usuario