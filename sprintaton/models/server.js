const express = require('express')
const conectar = require('../database/configuracion')
const Usuario = require('./usuario')
const cors = require('cors')

class Server{

  app = express()

  constructor(){
    this.conectarBD()
    this.app.use(cors())
    this.routes()
    this.app.listen(3000, ()=>{ console.log("Corriendo el servidor")})
  }

  async conectarBD(){
    await conectar()
  }

  routes(){
    
    this.app.post('/crear-usuario', async function (req, res) {
      const obj = req.body
      await Usuario.create(obj)
      res.send(obj)
    })

    this.app.get('/get-usuarios', async function (req, res) {
      const usuarios = await Usuario.find()
      res.send(usuarios)
    })

  }

}

module.exports = Server